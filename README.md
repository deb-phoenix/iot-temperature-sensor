# IOT Temperature Sensor
Le projet consiste à lire les données d'un capteur de température (DS18B20 Temperature Sensors), puis à les afficher sur un téléphone portable via une connexion Wi-FI et au protocole HTTP. Une fois les données récupérées, un mécanisme conditionnel est mis en place pour contrôler l'allumage de leds en fonction des variations de températures relevées.


## Table des matières
- [IOT Temperature Sensor](#iot-temperature-sensot)
  - [Table des matières](#table-des-matières)
  - [Documentation](#installation)
  - [Auteurs](#auteur)
  - [Licence](#licence)


## Documentation
- [Documentation](doc.pdf)
- [Présentation - Google Slide](https://docs.google.com/presentation/d/1_iBzGBTCjgs4uzHhTGoeIduBh_BS-9tf2Mtfr8A8Gq0/edit?usp=sharing)


## Auteurs
Ce projet a été développé par [_Baptiste_](https://gitlab.com/azdra), [_Déb_](https://gitlab.com/deb-phoenix) et [_Kévin_](https://gitlab.com/lhs-kevin-grand).


## Licence
Ce projet est sous licence [MIT](LICENSE). Vous pouvez consulter le fichier [LICENSE](LICENSE) pour plus de détails.
