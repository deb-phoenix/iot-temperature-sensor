document.addEventListener('DOMContentLoaded', () => {
    setInterval(() => {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("temperature").innerHTML = this.responseText;
                document.getElementById('temperature-icon').src = this.responseText >= 28.5 ? "temperature-hot.png" : "temperature-cold.png"
            }
        };
        xhttp.open("GET", "/temperature", true);
        xhttp.send();
    }, 2000) ;

    const toogleBluLED = document.getElementById('toogleBluLED')
    if (toogleBluLED) {
        toogleBluLED.addEventListener('click', () => {
            console.log('click');
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", "/toogleBluLED", true);
            xhttp.send();
        })
    }
})