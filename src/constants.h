#include <WiFi.h>

// BUS VARIABLE
const int ONE_WIRE_BUS = 4;
const int GREEN_BUS = 23;
const int RED_BUS = 21;
const int BLUE_BUS = 19;

// Timer variables
unsigned long lastTime = 0;  
unsigned long timerDelay = 1000;

// Replace with your network credentials
const char* ssid = "La Sainte";
const char* password = "jesuisunmdp";

// Temp variables
const int MAX_TEMP = 28.5;

// Html FILE
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <style>
    html {
     font-family: Arial;
     display: inline-block;
     margin: 0px auto;
     text-align: center;
    }
    h2 { font-size: 3.0rem; }
    p { font-size: 3.0rem; margin-bottom:1rem; }
    .units { font-size: 1.2rem; }
    .ds-labels{
      font-size: 1.5rem;
      vertical-align:middle;
      padding-bottom: 15px;
    }
    .parent {
      flex-direction: column;
      min-height: 100vh;
      width: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
    }
    #temperature {
      font-size: 2rem;
    }
    .parent-button {
      margin-top: 1rem;
    }
  </style>
</head>
<body>
  <div class="parent">
    <div>
      <p>
        <i class="fas fa-thermometer-half" style="color:#059e8a;"></i> 
        <span class="ds-labels">Temperature</span> 
      </p>
      <span id="temperature">0</span>
      <sup class="units">&deg;C</sup>
    </div>
    <div class="parent-button">
      <button id="toogleBluLED" class="btn btn-primary">Allumer/Eteindre LED bleu</button>
     </div>
  </div>
</body>
<script>
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("temperature").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/temperature", true);
  xhttp.send();
}, 500) ;
document.getElementById('toogleBluLED').addEventListener('click', () => {
  console.log('click');
  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", "/toogleBluLED", true);
  xhttp.send();
})
</script>
</html>)rawliteral";