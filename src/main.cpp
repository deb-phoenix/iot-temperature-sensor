#include "constants.h"
#include <Arduino.h>

// Import required libraries
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SPIFFS.h>

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature sensor 
DallasTemperature sensors(&oneWire);

// Variables to store temperature values
String temperature = "";

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

String readDSTemperature() {
  sensors.requestTemperatures(); 
  float tempC = sensors.getTempCByIndex(0);

  if(tempC == DEVICE_DISCONNECTED_C) {
    Serial.println("Failed to read from DS18B20 sensor");
    return "--";
  } 

  Serial.print("Temperature: ");
  Serial.println(tempC); 
  
  digitalWrite(RED_BUS, tempC >= MAX_TEMP ? HIGH : LOW);
  digitalWrite(GREEN_BUS, tempC >= MAX_TEMP ? LOW : HIGH);

  return String(tempC);
}

void connectToWiFi() {
  WiFi.begin(ssid, password);
  Serial.println("Connecting to WiFi");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
}

void setupWebRoutes() {
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html", "text/html");
    // request->send_P(200, "text/html", index_html);
  });
  server.on("/script.js", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/script.js", "text/javascript");
  });
  server.on("/bootstrap.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/bootstrap.css", "text/css");
  });
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/style.css", "text/css");
  });
  server.on("/temperature-cold.png", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/temperature-cold.png", "image/png");
  });
  server.on("/temperature-hot.png", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/temperature-hot.png", "image/png");
  });
  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", temperature.c_str());
  });
  server.on("/toogleBluLED", HTTP_POST, [](AsyncWebServerRequest *request){
    digitalWrite(BLUE_BUS, !digitalRead(BLUE_BUS));
    request->send_P(200, "text/plain", temperature.c_str());
  });
}

void initPins() {
  pinMode(GREEN_BUS, OUTPUT);
  pinMode(RED_BUS, OUTPUT);
  pinMode(BLUE_BUS, OUTPUT);
}

void initSPIFFS() {
  if (!SPIFFS.begin()) {
    Serial.println("Error initializing SPIFFS...");
    return;
  }
  Serial.println("SPIFFS initialized successfully.");
}

void setup() {
  Serial.begin(115200);
  Serial.println("\n");

  initPins();
  initSPIFFS();

  sensors.begin();
  connectToWiFi();

  Serial.println(WiFi.localIP());

  setupWebRoutes();
  server.begin();
}

void loop(){
  if ((millis() - lastTime) > timerDelay) {
    temperature = readDSTemperature();
    lastTime = millis();
  }  
}